package com.example.futbalmaniangoal;

public class TEamDEtails {


    public TEamDEtails(String strTeam, String intFormedYear, String strLeague, String strDescriptionIT, String strTeamBadge) {
        this.strTeam = strTeam;
        this.intFormedYear = intFormedYear;
        this.strLeague = strLeague;
        this.strDescriptionIT = strDescriptionIT;
        this.strTeamBadge = strTeamBadge;
    }
    private  String strTeam;
    private  String intFormedYear;
    private  String strLeague;
    private  String strDescriptionIT;
    private  String strTeamBadge;

    public String GetTeam(){
        return strTeam;
    }
    private  void SetTeam(String Team){
        this.strTeam =  Team;
    }

    public  String GetYear(){
        return intFormedYear;
    }
    private  void SetYear(String Year){
        this.intFormedYear =  Year;
    }

    public  String GetLeague(){
        return strLeague;
    }
    private  void SetLeague(String Leag){
        this.strLeague =  Leag;
    }

    public  String GetDescriptionIT(){
        return strDescriptionIT;
    }
    private  void SetDescriptionIT(String Description){
        this.strDescriptionIT =  Description;
    }
    private  String GetTeamBadge(){
        return strTeamBadge;
    }
    private  void SetTeamBadge(String TeamBadge){
        this.strTeamBadge =  TeamBadge;
    }

}
