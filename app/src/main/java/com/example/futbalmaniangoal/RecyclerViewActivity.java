package com.example.futbalmaniangoal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {
    private static  final  String TAG = "RecyclerView";
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImagieurl = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        Log.d(TAG, "onCreate: started");
        initImageBitmaps();

    }

    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps");
        mImagieurl.add("https://i.pinimg.com/originals/d3/83/95/d38395508b4f2eb38cfd2005a6d2b84d.jpg");
        mNames.add("Messi");
        mImagieurl.add("https://www.google.com/search?biw=1920&bih=989&tbm=isch&sa=1&ei=PpbuXNGSJIXyapHDjdgG&q=football+wallpaper+ronaldo&oq=football+wallpaper+ronaldo&gs_l=img.3..0j0i8i30l4.4657.6381..6905...0.0..0.98.728.8......0....1..gws-wiz-img.......0i67.Diq8aljP7HE#imgrc=G_FVPxpqmgLPpM:");
        mNames.add("Ronaldo");
        intricaiclerView();
    }
    private void intricaiclerView(){
        Log.d(TAG, "intricaiclerView: init Recaicil view.");
        RecyclerView recyclerView = findViewById(R.id.recyclerV);
        RecaiclarViewAdapter adapter = new RecaiclarViewAdapter(this,mNames,mImagieurl);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}
