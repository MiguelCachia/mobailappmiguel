package com.example.futbalmaniangoal;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecaiclarViewAdapter extends RecyclerView.Adapter<RecaiclarViewAdapter.ViewHolder> {
    private static  final  String TAG = "RecaiclarViewAdapter";

    private ArrayList<String> mImagiesNames = new ArrayList<>();
    private ArrayList<String> mImagies = new ArrayList<>();
    private Context mContext;

    public RecaiclarViewAdapter( Context context,ArrayList<String> imagiesNames, ArrayList<String> imagies) {
        this.mImagiesNames = imagiesNames;
        this.mImagies = imagies;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitiem,parent,false);
        ViewHolder Holder = new ViewHolder(view);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");

        Glide.with(mContext).asBitmap().load(mImagies.get(position)).into(holder.image);
        holder.imagename.setText(mImagiesNames.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View view){
                Log.d(TAG, "onClick: Clicked on:"+mImagies.get(position));
                Toast.makeText(mContext,mImagiesNames.get(position),Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {

        return mImagiesNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView imagename;
                RelativeLayout parentLayout;
        CircleImageView image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imagename = itemView.findViewById(R.id.imagename);
            parentLayout = itemView.findViewById(R.id.parent_Layout);
            image = itemView.findViewById(R.id.image);

        }
    }

}
