package com.example.futbalmaniangoal;

public class PlayerObject {
    public PlayerObject(String strPlayer, String strTeam, String strWage, String strDescriptionEN, String strNumber) {
        this.strPlayer = strPlayer;
        this.strTeam = strTeam;
        this.strWage = strWage;
        this.strDescriptionEN = strDescriptionEN;
        this.strNumber = strNumber;
    }

    String strPlayer;
    String strTeam;
    String strWage;
    String strDescriptionEN;
    String strNumber;

    public String GetPlayer(){
        return strPlayer;
    }
    private  void SetPlayer(String Player){
        this.strPlayer =  Player;
    }
    public String GetTeam(){
        return strTeam;
    }
    private  void SetTeam(String Team){
        this.strTeam =  Team;
    }


    public String GetWage(){
        return strWage;
    }
    private  void SetWage(String Wage){
        this.strWage =  Wage;
    }
    public String GetDescription(){
        return strDescriptionEN;
    }

    public String GetNumber(){
        return strNumber;
    }



}
