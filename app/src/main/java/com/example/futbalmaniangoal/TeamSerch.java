package com.example.futbalmaniangoal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;


public class TeamSerch extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    protected void onStop() {
        // call the superclass method first
        super.onStop();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        EditText Input = findViewById(R.id.Input);
        editor.putString("TeamName", Input.getText().toString());
        editor.apply();
    }

    @Override
    protected void onStart() {
        // call the superclass method first
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        //String text = sharedPreferences.getString("hello", "");
        String TeamNAme = sharedPreferences.getString("TeamName", "");
        EditText Input = findViewById(R.id.Input);
        Input.setText(TeamNAme);




    }

    @Override
    protected void onPause() {
        // call the superclass method first
        super.onPause();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        EditText Input = findViewById(R.id.Input);
        editor.putString("TeamName", Input.getText().toString());
        editor.apply();
    }

    @Override
    protected void onResume() {
        // call the superclass method first
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        //String text = sharedPreferences.getString("hello", "");
        String TeamNAme = sharedPreferences.getString("TeamName", "");
        EditText Input = findViewById(R.id.Input);
        Input.setText(TeamNAme);


        //
    }

    String TotalDetails = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_serch);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        //TamSerchHandiling handle = new TamSerchHandiling();
       // TEamDEtails team = handle.doInBackground("Arsenal");
        //TextView textView = (TextView) findViewById(R.id.Name);
       // textView.setText(team.GetTeam());




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.team_serch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Teamserch) {
            Toast.makeText(TeamSerch.this, "your already here!" , Toast.LENGTH_SHORT).show();
            // Handle the camera action
        } else if (id == R.id.ViewTeamPlayers) {
            finish();
            startActivity(new Intent(getApplicationContext(),ViewTPlayers.class));

        } else if (id == R.id.Viewcontryleags) {
            finish();
            startActivity(new Intent(getApplicationContext(),ViewTPlayers.class));


        } else if (id == R.id.ViewLEagTeams) {
            finish();
            startActivity(new Intent(getApplicationContext(),ViewTeamonLEag.class));
        }
        else if (id == R.id.Playerserch) {
            finish();
            startActivity(new Intent(getApplicationContext(),PlayerSerch.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @SuppressLint("SetTextI18n")

    public void getTeam(View v) {
        TextView Output = (TextView) findViewById(R.id.TeamName);
        EditText Input = findViewById(R.id.Input);
        ArrayList<TEamDEtails> Teams = new ArrayList<>();
        Output.setText("");
        TamSerchHandiling task = new TamSerchHandiling();


        try {
            Teams = task.execute(Input.getText().toString()).get();

            Output.setText("");

            for (int i = 0; i < Teams.size(); i++) {

                String teamm = Teams.get(i).GetTeam();
                String Year = Teams.get(i).GetYear();
                String League = Teams.get(i).GetLeague();
                String Description = Teams.get(i).GetDescriptionIT();
                TotalDetails = TotalDetails +"\n \nTEAM : "+teamm+"\n Year Astablished: "+Year+"\nLeag: "+League+"\nDescription"+Description;
                //Toast.makeText(this,TotalDetails ,Toast.LENGTH_SHORT).show();

            }
            Output.setText(TotalDetails);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }






    }

}
