package com.example.futbalmaniangoal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Calendar;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
        public static final String SHARED_PREFS = "sharedPrefs";
        public String hello;
    ConstraintLayout ConstraintLayout ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    protected void onStop() {
        // call the superclass method first
        super.onStop();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("hello", "Miguel");
        editor.putString("Date", String.valueOf(Calendar.getInstance().getTime()));
        editor.apply();
    }

    @Override
    protected void onStart() {
        // call the superclass method first
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        //String text = sharedPreferences.getString("hello", "");
        String date = sharedPreferences.getString("Date", "");

        Snackbar snackbar = Snackbar.make(findViewById(R.id.drawer_layout), date, Snackbar.LENGTH_LONG);

        snackbar.show();
        //Toast.makeText(Home.this,text + " " + date , Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        // call the superclass method first
        super.onPause();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("hello", "Miguel");
        editor.putString("Date", String.valueOf(Calendar.getInstance().getTime()));
        editor.apply();
    }

    @Override
    protected void onResume() {
        // call the superclass method first
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("user info", Context.MODE_PRIVATE);
        //String text = sharedPreferences.getString("hello", "");
        String date = sharedPreferences.getString("Date", "");
        Snackbar snackbar = Snackbar.make(findViewById(R.id.drawer_layout), date, Snackbar.LENGTH_LONG);

        snackbar.show();

      //
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Teamserch) {
            finish();
               startActivity(new Intent(getApplicationContext(),TeamSerch.class));
            // Handle the camera action
        } else if (id == R.id.ViewTeamPlayers) {
            finish();
            startActivity(new Intent(getApplicationContext(),ViewTPlayers.class));

        } else if (id == R.id.Viewcontryleags) {
            finish();
            startActivity(new Intent(getApplicationContext(),ViewCountryLeags.class));

        } else if (id == R.id.ViewLEagTeams) {

        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
