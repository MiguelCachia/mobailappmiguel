package com.example.futbalmaniangoal;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Pinview pinview = (Pinview) findViewById(R.id.PinView);
        if (pinview != null) {

            pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
                public void onDataEntered(Pinview pinview, boolean b) {
                    if(pinview.getValue().equals("1234")){

                        Toast.makeText(MainActivity.this, "" + pinview.getValue(), Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(getApplicationContext(),Home.class));
                    }
                    else{
                        Toast.makeText(MainActivity.this, "Fail!!" , Toast.LENGTH_SHORT).show();
                    }


                }


            });
        }
    }
}